# Route In Modal

**SUMMARY**

Route in modal adds the ability to define routes that will be rendered within a 
modal window.

- Define one or multiple routes
- Submit the opened forms via AJAX
- Show errors in the dialog without reloading
- Optionally display confirmation messages within a modal window

**Current maintainers**
* [økse](https://www.drupal.org/user/3560284)
* [dataweb](https://www.drupal.org/user/2873593)
* [tim-diels](https://www.drupal.org/user/2915097)
