(function ($, Drupal, drupalSettings, window) {
  'use strict';

  Drupal.behaviors.routeInModal = {
    attach: function () {
      Drupal.AjaxCommands.prototype.routeInModalRefreshPage = function () {
        window.location.reload();
      };
    }
  };

}(jQuery, Drupal, drupalSettings, window));
