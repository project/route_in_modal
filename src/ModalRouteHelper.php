<?php

namespace Drupal\route_in_modal;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * A helper class for managing configured routes.
 */
class ModalRouteHelper {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a ModalRouteHelper object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('route_in_modal.settings');
  }

  /**
   * Returns the configured routes.
   *
   * @return array
   *   The configured routes as array.
   */
  private function getRoutes(): array {
    $routes = $this->config->get('routes');

    return (empty($routes) ? [] : explode("\r\n", $routes));
  }

  /**
   * Check if route is configured to open in modal.
   *
   * @param string|null $route
   *   The route to check.
   *
   * @return false|array
   *   The route if the route is configured to open in modal, false otherwise.
   */
  public function routeHasModal(?string $route) {
    $configuredRoutes = $this->getRoutes();

    foreach ($configuredRoutes as $configuredRoute) {
      $extractedRoute = $this->extractRoute($configuredRoute);

      if ($route == $extractedRoute['route']) {
        return $extractedRoute['parameters'];
      }
    }

    return FALSE;
  }

  /**
   * Extract a route to the correct format to make use of parameters.
   *
   * @param string|null $route
   *   The route to extract.
   *
   * @return array
   *   The extracted route with the route and optional parameters.
   */
  private function extractRoute(?string $route): array {
    $parameters = [];

    if (strpos($route, '|')) {
      $routeParameters = explode(',', substr($route, strrpos($route, '|') + 1));

      foreach ($routeParameters as $routeParameter) {
        if (strpos($routeParameter, ':') !== FALSE) {
          [$key, $value] = explode(':', $routeParameter);
          $parameters[$key] = $value;
        }
      }

      $route = substr($route, 0, strrpos($route, '|'));
    }

    return ['route' => $route, 'parameters' => $parameters];
  }

}
