<?php

namespace Drupal\route_in_modal;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\StatusMessages;
use Drupal\route_in_modal\AjaxCommand\RefreshPageCommand;

/**
 * A helper class for creating Ajax responses for Route In Modal.
 */
class RouteInModalAjaxHelper {

  /**
   * The callback for the AJAX call.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response.
   */
  public static function ajaxCallback(array $form, FormStateInterface $formState): AjaxResponse {

    $config = \Drupal::config('route_in_modal.settings');
    $messenger = \Drupal::messenger();
    $messages = $messenger->all();
    $response = new AjaxResponse();

    if (!isset($messages['error'])) {

      $response->addCommand(new CloseModalDialogCommand());

      $messages_modal = $config->get('confirmation_messages_modal');
      if ($messages_modal && !empty($messages)) {

        $options = [
          'width' => $config->get('dialog_width') ?? '480',
          'height' => $config->get('dialog_height') ?? 'auto',
          'dialogClass' => 'views-ui-dialog js-views-ui-dialog',
        ];

        $messages = StatusMessages::renderMessages();
        $response->addCommand(new OpenModalDialogCommand('', $messages, $options));
      }

      $response->addCommand(self::redirectCommand($formState));
    }
    else {
      /** @var \Drupal\Core\Render\RendererInterface $renderer */
      $renderer = \Drupal::service('renderer');

      /** @var \Drupal\Core\Extension\ModuleHandler $moduleHandler */
      $moduleHandler = \Drupal::service('module_handler');
      if ($moduleHandler->moduleExists('inline_form_errors')) {
        // Refresh form with inline errors.
        $response->addCommand(new HtmlCommand('#route_in_modal_wrapper', $form));

        // Delete default messages.
        $messenger->deleteAll();
      }
      else {
        $messagesElement = [
          '#type' => 'container',
          '#attributes' => [
            'class' => 'route-in-modal-messages',
          ],
          'messages' => ['#type' => 'status_messages'],
        ];

        $response->addCommand(new RemoveCommand('.route-in-modal-messages'));

        $response->addCommand(new PrependCommand(
          '#route_in_modal_wrapper',
          $renderer->renderRoot($messagesElement)
        ));
      }
    }

    return $response;
  }

  /**
   * Redirect command for the form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\RedirectCommand|\Drupal\route_in_modal\AjaxCommand\RefreshPageCommand
   *   The AJAX response.
   */
  public static function redirectCommand(FormStateInterface $formState) {
    global $base_url;

    $config = \Drupal::config('route_in_modal.settings');

    return $config->get('reload_on_success')
      ? new RefreshPageCommand()
      : new RedirectCommand($base_url . $formState->getRedirect());
  }

}
