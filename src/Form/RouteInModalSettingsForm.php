<?php

namespace Drupal\route_in_modal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The admin settings form for Route In Modal.
 */
class RouteInModalSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'route_in_modal_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['route_in_modal.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('route_in_modal.settings');

    $form['routes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Add routes to be opened in a modal'),
      '#description' => $this->t('One route per line. Optional parameters can be added by separating the route with a pipe sign. Individual parameters should be separated with a comma sign. e.g. |width:480,height:auto'),
      '#default_value' => $config->get('routes'),
    ];

    $form['dialog_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dialog width'),
      '#description' => $this->t('The value should either be the width of the modal window in pixels, or "auto". The default is 480.'),
      '#default_value' => $config->get('dialog_width'),
    ];

    $form['dialog_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dialog height'),
      '#description' => $this->t('The value should either be the height of the modal window in pixels, or "auto". The default is auto.'),
      '#default_value' => $config->get('dialog_height'),
    ];

    $form['confirmation_messages_modal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Confirmation messages within the modal'),
      '#description' => $this->t('Enable the display of confirmation messages within the modal instead of the default Drupal messenger service.'),
      '#default_value' => $config->get('confirmation_messages_modal'),
    ];

    $form['reload_on_success'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reload on success'),
      '#description' => $this->t('Reload the page (instead of redirecting) upon completion.'),
      '#default_value' => $config->get('reload_on_success'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('route_in_modal.settings')
      ->set('routes', $form_state->getValue('routes'))
      ->set('dialog_width', $form_state->getValue('dialog_width'))
      ->set('dialog_height', $form_state->getValue('dialog_height'))
      ->set('confirmation_messages_modal', $form_state->getValue('confirmation_messages_modal'))
      ->set('reload_on_success', $form_state->getValue('reload_on_success'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
