<?php

namespace Drupal\route_in_modal\AjaxCommand;

use Drupal\Core\Ajax\CommandInterface;

/**
 * An Ajax Command that refreshes the current page.
 */
class RefreshPageCommand implements CommandInterface {

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    return [
      'command' => 'routeInModalRefreshPage',
    ];
  }

}
